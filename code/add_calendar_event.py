import sys

from oauth2client import client
from googleapiclient import sample_tools
import argparse
from datetime import date, datetime

def main(argv):
    
    calendar_id = 'tvj3mghoha5gf6bvsrmqe7fi7k@group.calendar.google.com'
    summary = 'Demo Event'
    description = 'This is a test.'
    location = 'N-Hotel'
    date = 'October 25, 2015'
    start_time = '13:00:00'
    end_time = '15:00:00'
    
    # Authenticate and construct service.
    service, flags = sample_tools.init(argv, 'calendar', 'v3', __doc__, __file__, scope='https://www.googleapis.com/auth/calendar')
    
    try:
        page_token = None
        
        #setup the event to create
        event_entry = {
            'summary': summary,
            'location': location,
            'description': description,
            'start': {
                'dateTime': datetime.strptime(''.join((date, ' ', start_time)), "%B %d, %Y %H:%M:%S").isoformat(),
                'timeZone': 'Asia/Manila'
            },
            'end': {
                'dateTime': datetime.strptime(''.join((date, ' ', end_time)), "%B %d, %Y %H:%M:%S").isoformat(),
                'timeZone': 'Asia/Manila'
            }
        }
        
        event = service.events().insert(calendarId=calendar_id, body=event_entry).execute()
        print 'Event created: %s' % (event.get('htmlLink'))
    
    except client.AccessTokenRefreshError:
        print ('The credentials have been revoked or expired, please re-run the application to re-authorize.')

if __name__ == '__main__':
  main(sys.argv)