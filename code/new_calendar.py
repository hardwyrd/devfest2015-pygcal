import sys

from oauth2client import client
from googleapiclient import sample_tools

def main(argv):
    
  # Authenticate and construct service.
  service, flags = sample_tools.init(
      argv, 'calendar', 'v3', __doc__, __file__,
      scope='https://www.googleapis.com/auth/calendar')
  
  try:
      
    page_token = None
    
    calendar = {
        'summary': 'myCalendar',
        'timeZone': 'Asia/Manila'
    }
    
    created_calendar = service.calendars().insert(body=calendar).execute()
    
    print('Calendar {} is created.').format(created_calendar['id'])
    

  except client.AccessTokenRefreshError:
    print ('The credentials have been revoked or expired, please re-run'
      'the application to re-authorize.')

if __name__ == '__main__':
  main(sys.argv)